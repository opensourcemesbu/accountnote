package com.mesbu.moneyaccountmanagement.db; 
import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.dsl.MultiUnique;
import com.orm.dsl.Table;

@Table(name = "TagAccountType")
@MultiUnique("Id")
public class db_TagAccountType extends SugarRecord {


    @Column(name = "TagId")
    public int TagId;

    @Column(name = "AccountTypeId")
    public int AccountTypeId;

    public db_TagAccountType() {

    }

    public db_TagAccountType(
            int TagId, int AccountTypeId

    ) {
        this.TagId = TagId;
        this.AccountTypeId = AccountTypeId;
    }

    public db_AccountType getAccountType() {
        return db_ManagerGlobal.getAccountTypeById(this.AccountTypeId);
    }

    public db_Tag getTagId() {
        return db_ManagerGlobal.getTagById(this.TagId);
    }
}