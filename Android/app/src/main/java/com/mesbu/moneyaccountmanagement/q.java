package com.mesbu.moneyaccountmanagement;

import android.database.sqlite.SQLiteDatabase;

import com.mesbu.moneyaccountmanagement.db.d_Inst;
import com.orm.SchemaGenerator;
import com.orm.SugarContext;
import com.orm.SugarDb;

import java.io.File;
import java.util.Locale;

public class q {

    public static  MainActivity main;

    public static SQLiteDatabase db=null;

    public static void StartDb() {
        boolean Setup=false;
        if(!q.FileExists( q.main.getDatabasePath("account_note.db").getPath()))
        {
            d_Inst.Setup(q.main.getDatabasePath("account_note.db").getPath());
        }
        SugarContext.init(main.getApplicationContext());
        SchemaGenerator schemaGenerator = new SchemaGenerator(main);
        q.db=new SugarDb(main).getDB();

        schemaGenerator.createDatabase(q.db);
    }

    public static boolean FileExists(String filePath)
    {
        File file = new File(filePath);
        return  file.exists();

    }
    public static String getLangText(int i)
    {
        return q.main.getResources().getString(i);
    }
    public static String getDeviceLang()
    {
        return Locale.getDefault().getDisplayLanguage().toString();
    }
}
