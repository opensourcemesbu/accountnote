package com.mesbu.moneyaccountmanagement.db;
import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.dsl.MultiUnique;
import com.orm.dsl.Table;

@Table(name = "StartWindow")
@MultiUnique("Code")
public class db_StartWindow extends SugarRecord {

    @Column(name = "Code")
    public String Code;

    @Column(name = "AutoLogin")
    public int AutoLogin;
    @Column(name = "MasterPass",notNull = true)
    public String MasterPass;

    @Column(name = "LoginUserId",notNull = false)
    public int LoginUserId;

    @Column(name = "SQLVersiyon")
    public int SQLVersiyon;

    @Column(name = "CDate")
    public float CDate;

    public db_StartWindow() {

    }

    public db_StartWindow(
            String Code,
            Integer AutoLogin,
            String MasterPass,
            Integer LoginUserId,
            float CDate,long ID,int SQLVersiyon

    ) {
        this.Code = Code;
        this.AutoLogin = AutoLogin;
        this.MasterPass = MasterPass;
        this.LoginUserId = LoginUserId;
        this.CDate = CDate;
        this.SQLVersiyon = SQLVersiyon;
        this.setId(ID);
    }
}
