package com.mesbu.moneyaccountmanagement.db; 
import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.dsl.MultiUnique;
import com.orm.dsl.Table;

@Table(name = "Tag")
@MultiUnique("Id")
public class db_Tag extends SugarRecord {

    @Column(name = "TagName")
    public String TagName;

    @Column(name = "UserId")
    public int UserId;

    @Column(name = "CDate")
    public float CDate;

    @Column(name = "OperationTypeId")
    public int OperationTypeId;


    public db_Tag() {

    }

    public db_Tag(
            String TagName,
            int UserId,
            String OpName,
            float Factor

    ) {
        this.TagName = TagName;
        this.UserId = UserId;
        this.CDate = CDate;
        this.OperationTypeId = OperationTypeId;
    }

    /**
     * Read UserClass
     *
     * */
    public db_Users getUser() {
        return db_ManagerGlobal.getUserId(this.UserId);
    }
    /**
     * Read OperationType
     *
     * */
    public db_OperationType getOperationType() {
        return db_ManagerGlobal.getOperationTypeById(this.OperationTypeId);
    }

    /**
     * User Change Cache Remove
     * Kullanıcıda değişiklik Yapıldığında önbeleği silme
     * **/
    @Override
    public long save() {
        long i=super.save();
        if(super.save()>0)
        {
            db_ManagerGlobal.removeCacheTagId(this.getId());
        }
        return i;
    }


}