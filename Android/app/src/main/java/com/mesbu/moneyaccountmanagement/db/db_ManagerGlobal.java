package com.mesbu.moneyaccountmanagement.db;

import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.LinkedHashMap;

public class db_ManagerGlobal {

    // region Db Cache Manger
    // region OperationType Manager
    private static LinkedHashMap<Integer,db_OperationType> ListOperationType=null;
    public static db_OperationType getOperationTypeById(int Id)
    {
        if(ListOperationType==null)
            ListOperationType=new LinkedHashMap<Integer, db_OperationType>();
        if(!ListOperationType.containsKey(Id))
        {
            db_OperationType obj= Select.from(db_OperationType.class)
                    .where(Condition.prop("ID").eq(Id) )
                    .first();
            ListOperationType.put(Id,obj);
        }
        return  ListOperationType.get(Id);

    }
    // endregion

    // region db_AccountType Manager
    private static LinkedHashMap<Integer,db_AccountType> ListAccountType=null;
    public static db_AccountType getAccountTypeById(int Id)
    {
        if(ListAccountType==null)
            ListAccountType=new LinkedHashMap<Integer, db_AccountType>();

        if(!ListAccountType.containsKey(Id))
        {
            db_AccountType obj= Select.from(db_AccountType.class)
                    .where(Condition.prop("ID").eq(Id) )
                    .first();
            ListAccountType.put(Id,obj);
        }
        return  ListAccountType.get(Id);

    }
    // endregion

    // region db_Users Manager
    private static LinkedHashMap<Integer,db_Users> ListUser=null;

    public static db_Users getUserId(int UserId)
    {
        if(ListUser==null)
            ListUser=new LinkedHashMap<Integer, db_Users>();
        if(!ListUser.containsKey(UserId))
        {
            db_Users user=Select.from(db_Users.class)
                    .where(Condition.prop("ID").eq(UserId) )
                    .first();
            ListUser.put(UserId,user);
        }
        return  ListUser.get(UserId);

    }

    public static void removeCacheUserId(Long id) {
        if(ListUser.containsKey(id))
        {
            ListUser.remove(id);
        }
    }
    // endregion

    // region db_Account Manager
    private static LinkedHashMap<Integer,db_Account> ListAccount=null;

    public static db_Account getAccountById(int accountTypeId)
    {
        if(ListAccount==null)
            ListAccount=new LinkedHashMap<Integer, db_Account>();
        if(!ListAccount.containsKey(accountTypeId))
        {
            db_Account obj=Select.from(db_Account.class)
                    .where(Condition.prop("ID").eq(accountTypeId) )
                    .first();
            ListAccount.put(accountTypeId,obj);
        }
        return  ListAccount.get(accountTypeId);

    }

    public static void removeCacheAccountId(Long id) {
        if(ListAccount.containsKey(id))
        {
            ListAccount.remove(id);
        }
    }
    // endregion

    // region db_Tag Manager
    private static LinkedHashMap<Integer,db_Tag> ListTag=null;
    public static db_Tag getTagById(int tagId)
    {
        if(ListTag==null)
            ListTag=new LinkedHashMap<Integer, db_Tag>();
        if(!ListTag.containsKey(tagId))
        {
            db_Tag obj= Select.from(db_Tag.class)
                    .where(Condition.prop("ID").eq(tagId) )
                    .first();
            ListTag.put(tagId,obj);
        }
        return  ListTag.get(tagId);

    }

    public static void removeCacheTagId(Long id) {
        if(ListTag.containsKey(id))
        {
            ListTag.remove(id);
        }
    }
    // endregion
    // endregion
    //region db_StartWindow
    private static db_StartWindow db_startWindow=null;
    public static db_StartWindow getStartView()
    {
        if(db_startWindow==null){
            db_startWindow= Select.from(db_StartWindow.class)
                    .where(Condition.prop("Code").eq("Setup") )
                    .first();
        }
        return  db_startWindow;
    }
    //endregion
}
