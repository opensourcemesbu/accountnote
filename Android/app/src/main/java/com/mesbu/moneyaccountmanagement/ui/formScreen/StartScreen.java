package com.mesbu.moneyaccountmanagement.ui.formScreen;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

import com.mesbu.moneyaccountmanagement.R;
import com.mesbu.moneyaccountmanagement.db.*;
import com.mesbu.moneyaccountmanagement.lib.FormWindow;
import com.mesbu.moneyaccountmanagement.q;

/**
 * Başlangıç Ayarlarının Yapıldığı Ekran
 * Start Settings Form
 * */
public class StartScreen extends FormWindow {

    public StartScreen(Boolean BackButton) {

        super(R.layout.start_screen_main, BackButton);
        this.setTitle(R.string.start_title_start);
        if(db_ManagerGlobal.getStartView().CDate==0) {
            this.Show();

            Button btn_save = this.getIncludeView().findViewById(R.id.btn_save);
            btn_save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SaveForm();
                }
            });
        }
    }
    public void SaveForm()
    {
        EditText txUserName = this.getIncludeView().findViewById(R.id.username);
        EditText txPass = this.getIncludeView().findViewById(R.id.pass);
        EditText txPass2 = this.getIncludeView().findViewById(R.id.pass2);
        Switch swAutoLogin = this.getIncludeView().findViewById(R.id.swc_auto_login);

        if(txUserName.getText().toString().trim().length()<3)
        {
            txUserName.setError(q.getLangText(R.string.required_field));
            return;
        }else
            txUserName.setError(null);

        if(!txPass.getText().toString().equals(txPass2.getText().toString()))
        {
            txPass.setError(q.getLangText(R.string.password_does_not_match));
            txPass2.setError(q.getLangText(R.string.password_does_not_match));
        }else
        {
            txPass.setError(null);
            txPass2.setError(null);
        }



    }
}
