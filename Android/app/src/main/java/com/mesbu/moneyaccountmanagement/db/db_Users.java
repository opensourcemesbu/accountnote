package com.mesbu.moneyaccountmanagement.db; 
import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.dsl.Ignore;
import com.orm.dsl.MultiUnique;
import com.orm.dsl.Table;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Table(name = "Users")
@MultiUnique("Id")
public class db_Users extends SugarRecord {

    @Column(name = "UserName")
    public String UserName;

    @Column(name = "CDate")
    public float CDate;

    @Column(name = "Pass")
    public String Pass;

    @Column(name = "Lang", notNull = true)
    public String Lang;


    public db_Users() {

    }

    public db_Users(
            String UserName,
            String Pass,
            String Lang,
            float CDate

    ) {
        this.UserName = UserName;
        this.Pass = Pass;
        this.Lang = Lang;
        this.CDate = CDate;
    }
    /**
     * User Change Cache Remove
     * Kullanıcıda değişiklik Yapıldığında önbeleği silme
     * **/
    @Override
    public long save() {
        long i=super.save();
        if(super.save()>0)
        {
            this.getId();
            db_ManagerGlobal.removeCacheUserId(this.getId());
        }
        return i;
    }


}