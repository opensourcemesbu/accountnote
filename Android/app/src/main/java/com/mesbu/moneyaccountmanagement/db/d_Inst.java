package com.mesbu.moneyaccountmanagement.db;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;

import com.mesbu.moneyaccountmanagement.q;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;


public class d_Inst {
    public static void Setup(String dbPath) {


        String DATABASE_NAME = "account_note.db";
        InputStream mInput = null;
        try {
            mInput = q.main.getAssets().open(DATABASE_NAME);

            String outFileName = dbPath;
            OutputStream mOutput = new FileOutputStream(outFileName);
            byte[] mBuffer = new byte[2024];
            int mLength;
            while ((mLength = mInput.read(mBuffer)) > 0) {
                mOutput.write(mBuffer, 0, mLength);
            }
            mOutput.flush();
            mOutput.close();
            mInput.close();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
