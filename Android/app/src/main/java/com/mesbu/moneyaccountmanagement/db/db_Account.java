package com.mesbu.moneyaccountmanagement.db; 
import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.dsl.Ignore;
import com.orm.dsl.MultiUnique;
import com.orm.dsl.Table;
import com.orm.query.Condition;
import com.orm.query.Select;

@Table(name = "Account")
@MultiUnique("Id")
public class db_Account extends SugarRecord {

    /**
     * Account Name
     * Hesap Adı
     * */
    @Column(name = "Name")
    public String Name;
    /**
     * Curreny
     * Hesap Para Birimi
     * */
    @Column(name = "Curreny")
    public String Curreny;
    /**
     * UserId
     * Kullanıcı Adı
     * */
    @Column(name = "UserId")
    public int UserId;
    /**
     * Credit Card Account Cut Day
     * Hesap Kesim Günü
     * */
    @Column(name = "AccountCutDay")
    public int AccountCutDay;
    /**
     * Due Day
     * Vade(Son) Ödeme Tarihi
     * */
    @Column(name = "DueDay")
    public int DueDay;
    /**
     * Account Type
     * Hesap Tipi
     * 1 = Cash, 2 = Bank, 3 = CreditCard
     * 1 = Nakit, 2 = Banka,3 = KrediKartı
     * */
    @Column(name = "AccountTypeId")
    public int AccountTypeId;
    /**
     * Create Date
     * Oluşturma Tarihi
     * */
    @Column(name = "CDate")
    public float CDate;


    public db_Account() {

    }
    /**
     * New Account
     * Yeni Hesap
     * @param Name Account Name
     * @param Curreny Account Curreny
     * @param UserId UserId
     * @param AccountCutDay Account Cut Day
     * @param DueDay    Due Day
     * @param AccountTypeId Account Type
     * @param CDate   Create Date
     * */
    public db_Account(
            String Name,
            String Curreny,
            int UserId,
            int AccountCutDay,
            int DueDay,
            int AccountTypeId,
            float CDate

    ) {
        this.Name = Name;
        this.Curreny = Curreny;
        this.UserId = UserId;
        this.AccountCutDay = AccountCutDay;
        this.DueDay = DueDay;
        this.AccountTypeId = AccountTypeId;
        this.CDate = CDate;
    }

    /**
     * Database Read AccountType
     *
     * */
    public db_AccountType getAccountType()
    {
        return  db_ManagerGlobal.getAccountTypeById(this.AccountTypeId);
    }
    /**
     * Read UserClass
     *
     * */
    public db_Users getUser() {
        return db_ManagerGlobal.getUserId(this.UserId);
    }

    /**
     * Account Change Cache Remove
     * Hesap değişiklik Yapıldığında önbeleği silme
     * **/
    @Override
    public long save() {
        long i=super.save();
        if(super.save()>0)
        {
            this.getId();
            db_ManagerGlobal.removeCacheAccountId(this.getId());
        }
        return i;
    }
}