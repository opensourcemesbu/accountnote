package com.mesbu.moneyaccountmanagement.db; 
import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.dsl.MultiUnique;
import com.orm.dsl.Table;

@Table(name = "Tag")
@MultiUnique("Id")
public class db_AccountType extends SugarRecord {

    @Column(name = "TypeName")
    public String TypeName;
    @Column(name = "TypeKode")
    public String TypeKode;
    public db_AccountType() {

    }

    public db_AccountType(
            String TypeName,
            String TypeKode

    ) {
        this.TypeName = TypeName;
        this.TypeKode = TypeKode;
    }
}