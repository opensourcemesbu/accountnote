package com.mesbu.moneyaccountmanagement.db;
import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.dsl.MultiUnique;
import com.orm.dsl.Table;

@Table(name = "StartWindow")
@MultiUnique("Code")
public class db_Voucher extends SugarRecord {

    @Column(name = "OperationTypeId")
    public int OperationTypeId;

    @Column(name = "UserId")
    public int UserId;

    @Column(name = "TagId")
    public int TagId;

    @Column(name = "AccountId")
    public int AccountId;

    @Column(name = "Notes",notNull = true)
    public String Notes;

    @Column(name = "CDate")
    public float CDate;

    @Column(name = "Amount")
    public float Amount;

    @Column(name = "CYear")
    public int CYear;

    @Column(name = "CMonth")
    public int CMonth;

    @Column(name = "CDay")
    public int CDay;

    @Column(name = "CDayOfSeccond")
    public int CDayOfSeccond;

    @Column(name = "Installment",notNull = true)
    public int Installment;

    @Column(name = "InstallmentTotalAmount",notNull = true)
    public float InstallmentTotalAmount;

    @Column(name = "InstallmentTotal",notNull = true)
    public int InstallmentTotal;

    @Column(name = "InstallmentGuid",notNull = true)
    public String InstallmentGuid;


    public db_Voucher() {

    }

    public db_Voucher(
            int OperationTypeId,
            int UserId,
            int TagId,
            int AccountId,
            String Notes,
            float CDate,
            float Amount,
            int CYear,
            int CMonth,
            int CDay,
            int CDayOfSeccond,
            int Installment,
            float InstallmentTotalAmount,
            int InstallmentTotal ,
            String InstallmentGuid

    ) {
        this.OperationTypeId = OperationTypeId;
        this.UserId = UserId;
        this.TagId = TagId;
        this.AccountId = AccountId;
        this.Notes = Notes;
        this.CDate = CDate;
        this.Amount = Amount;
        this.CYear = CYear;
        this.CMonth = CMonth;
        this.CDay = CDay;
        this.CDayOfSeccond = CDayOfSeccond;
        this.Installment = Installment;
        this.InstallmentTotalAmount = InstallmentTotalAmount;
        this.InstallmentTotal = InstallmentTotal;
        this.InstallmentGuid = InstallmentGuid;
    }
    /**
     * Read UserClass
     *
     * */
    public db_Users getUser() {
        return db_ManagerGlobal.getUserId(this.UserId);
    }
    /**
     * Read OperationType
     *
     * */
    public db_OperationType getOperationType() {
        return db_ManagerGlobal.getOperationTypeById(this.OperationTypeId);
    }
    public db_Account getAccountType() {
        return db_ManagerGlobal.getAccountById(this.AccountId);
    }

    public db_Tag getTagId() {
        return db_ManagerGlobal.getTagById(this.TagId);
    }
}
