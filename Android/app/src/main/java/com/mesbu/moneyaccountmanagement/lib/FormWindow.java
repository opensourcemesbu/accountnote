package com.mesbu.moneyaccountmanagement.lib;


import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mesbu.moneyaccountmanagement.R;
import com.mesbu.moneyaccountmanagement.q;


public class FormWindow {

    protected Dialog _form;
    protected View _includeView;
    protected TextView _titleText;
    public View getIncludeView()
    {
        return  _includeView;
    }
    public FormWindow(int Custom_Layout, Boolean BackButton) {

        _form = new Dialog(q.main);//,android.R.style.Theme_Black_NoTitleBar); 
        _form.setContentView(R.layout.formwindow);
        this._titleText=_form.findViewById(R.id.txtTitleView);
        _includeView = LayoutInflater.from(q.main).inflate(Custom_Layout,null, false);
        LinearLayout mainView=(LinearLayout) _form.findViewById(R.id.mainViewLinearLayout);
        mainView.addView(_includeView);
        _form.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        _form.setCancelable(BackButton);
        _form.setCanceledOnTouchOutside(false);
    }
    public void Show() {
        this._form.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(_form.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.FILL_PARENT;
        lp.height = WindowManager.LayoutParams.FILL_PARENT;
        _form.getWindow().setAttributes(lp);

    }
    public String getTitle()
    {
        return this._titleText.getText().toString();
    }
    public FormWindow setTitle(String Title)
    {
        this._titleText.setText(Title);
        return this;
    }
    public FormWindow setTitle(int Title)
    {
        this._titleText.setText(Title);
        return this;
    }
    public FormWindow Hide() {
        this._form.hide();
        return this;
    }
    public FormWindow Cancel() {
        this._form.cancel();
        return this;
    }
    public void Disposes() {
        this._form.dismiss();

    }
}
