package com.mesbu.moneyaccountmanagement.db; 
import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.dsl.MultiUnique;
import com.orm.dsl.Table;

@Table(name = "OperationType")
@MultiUnique("Id")
public class db_OperationType extends SugarRecord  {

    @Column(name = "OpCode")
    public String OpCode;

    @Column(name = "OpName")
    public String OpName;

    @Column(name = "Factor")
    public float Factor;


    public db_OperationType() {

    }

    public db_OperationType(
            String OpCode,
            String OpName,
            float Factor

    ) {
        this.OpCode = OpCode;
        this.OpName = OpName;
        this.Factor = Factor;
    }

}